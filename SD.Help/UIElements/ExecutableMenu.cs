﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Menu;
using SD.Help.Models;
using SD.Help.Services;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace SD.Help.UIElements
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    public class ExecutableMenu : IMenu<ObjectsViewContext>
    {
        private BuilderService _builderObject;

        [ImportingConstructor]
        public ExecutableMenu(BuilderService builderObject)
        {
            _builderObject = builderObject;
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            builder.AddSeparator(builder.Count);
            builder.AddItem(nameof(ExecutableMenu), builder.Count).WithHeader("Сформировать объект");
        }

        public async void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            var obj = await _builderObject.Build<ObjectProject>(context.SelectedObjects.First());
        }
    }
}