﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SD.Help.Models
{
    [Description("IRD")]
    public class IRD
    {
        [Description("name")]
        public string Name { get; set; }

        public IList<IRDFolder> iRDFolders { get; set; }
    }
}