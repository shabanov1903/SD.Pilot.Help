﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SD.Help.Models
{
    [Description("_object_project")]
    public class ObjectProject
    {
        [Description("KISUP_code")]
        public string KisupCode { get; set; }
        
        [Description("name_object")]
        public string Name { get; set; }

        public IList<IRD> iRDs { get; set; }
        public IList<II> iIs { get; set; }
    }
}