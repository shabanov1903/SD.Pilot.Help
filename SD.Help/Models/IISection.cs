﻿using System.ComponentModel;

namespace SD.Help.Models
{
    [Description("section_ES")]
    public class IISection
    {
        [Description("name")]
        public string Name { get; set; }
    }
}