﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SD.Help.Models
{
    [Description("ES")]
    public class II
    {
        [Description("name")]
        public string Name { get; set; }

        public IList<IISection> iISections { get; set; }
    }
}