﻿using System.ComponentModel;

namespace SD.Help.Models
{
    [Description("folder_IRD")]
    public class IRDFolder
    {
        [Description("name")]
        public string Name { get; set; }
    }
}