﻿using Ascon.Pilot.SDK;
using SD.Help.Services;
using System.ComponentModel.Composition;

namespace SD.Help
{
    [Export(typeof(IDataPlugin))]
    public class DataPlugin : IDataPlugin
    {
        private IObjectsRepository _objectsRepository;
        private IObjectModifier _objectModifier;
        private IPilotServiceProvider _pilotServiceProvider;

        [ImportingConstructor]
        public DataPlugin(
            IObjectsRepository objectsRepository,
            IObjectModifier objectModifier,
            IPilotServiceProvider pilotServiceProvider)
        {
            _objectsRepository = objectsRepository;
            _objectModifier = objectModifier;
            _pilotServiceProvider = pilotServiceProvider;

            _pilotServiceProvider.Register(new BuilderService(_objectsRepository));
        }
    }
}