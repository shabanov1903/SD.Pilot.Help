﻿using Ascon.Pilot.SDK;
using Qwerta.Pilot.SDK.Extentions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;

namespace SD.Help.Services
{
    [Export(typeof(BuilderService))]
    public class BuilderService
    {
        private readonly IObjectsRepository _objectsRepository;

        [ImportingConstructor]
        public BuilderService(IObjectsRepository objectsRepository)
        {
            _objectsRepository = objectsRepository;
        }

        public async Task<T> Build<T>(IDataObject parent) where T : new()
        {
            var type = typeof(T);
            var attr = type.CustomAttributes.FirstOrDefault(s => s.AttributeType.Name == nameof(DescriptionAttribute));
            var code = attr.ConstructorArguments.First();
            var pilotType = _objectsRepository.GetType(parent.Type.Id);
            
            if (pilotType.Name != code.Value.ToString())
                return default(T);

            var result = new T();
            
            await SetFields(result, parent);

            return result;
        }

        private async Task SetFields<U>(U obj, IDataObject dataObject) where U : new()
        {
            var children = await _objectsRepository.GetObjectsAsync(dataObject.Children);
            var type = obj.GetType();

            foreach (var field in type.GetProperties())
            {
                var attr = field.CustomAttributes.FirstOrDefault(s => s.AttributeType.Name == nameof(DescriptionAttribute));
                if (attr != null)
                {
                    var code = attr.ConstructorArguments.First();
                    
                    if (dataObject.Attributes.TryGetValue(code.Value.ToString(), out object value))
                    {
                        field.SetValue(obj, value);
                    }
                }
                else
                {
                    var iListType = field.PropertyType.GetGenericArguments().First();
                    var iListAttr = iListType.CustomAttributes.FirstOrDefault(s => s.AttributeType.Name == nameof(DescriptionAttribute));
                    var iListCode = iListAttr.ConstructorArguments.First();
                    var iListPilotType = _objectsRepository.GetTypes().FirstOrDefault(t => t.Name == iListCode.Value.ToString());
                    
                    foreach(var child in children)
                    {
                        if (child.Type.Id == iListPilotType.Id)
                        {
                            var iListItem = Activator.CreateInstance(iListType);
                            await SetFields(iListItem, child);

                            IList list = (IList)field.GetValue(obj);
                            if (list == null)
                            {
                                var listType = typeof(List<>);
                                var constructedListType = listType.MakeGenericType(iListType);
                                list = (IList)Activator.CreateInstance(constructedListType);
                            }

                            list.Add(iListItem);
                            field.SetValue(obj, list);
                        }
                    }
                }
            }
        }
    }
}